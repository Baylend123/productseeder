import { faker } from '@faker-js/faker';
import {cats} from './product_category.js'
import Medusa from '@medusajs/medusa-js'
import { v4 as uuidv4 } from 'uuid';
import { AdminAuthResource } from '@medusajs/medusa-js';
import fs from 'fs'
const medusa = new Medusa(
    { baseUrl: 'http://localhost:9000', maxRetries: 3 , apiKey : '67DCDE3CB2FACDB9196FD4FD2335F'
})
import XLSX_json from 'xlsx-to-json';
let arr;
await XLSX_json({
  input: "test.xlsx", 
  output: "output.json"
}, function(err, result) {
  if(err) {
    console.error(err);
  }else {
    // console.log(result[0])


    const createProd = (item) => {
        if( item['DialedN PCAT'] !== 'Need to add'){
        
        let prod =  {
            "title":item.Title ? item.Title : '',
            "average_rating" : Math.floor(Math.random() * 500) / 100,
            "subtitle":item.Subtitle ? item.Subtitle : '',
            "material": item.Material ? item.Material : '',
            "handle": item.Handle ? item.Handle : '',
            "discountable":true,
            "is_giftcard":item['Gift Card'] === 'TRUE' ? true : false,
            "description":item['Body HTML'] ? item['Body HTML'] : '',
            "specs" : "<p>Specs Go Here </p>",
            "categories":[
                
            ],
            "tags" : [{value : `upc_${Math.floor(Math.random() * 100000000000)}`}, ...item.Tags.split(',').map(el => ({value : el}))],
            "options":[
                // {"title": item['Option1 Name']},
                // {"title": item['Option2 Name']},
                // {"title": item['Option3 Name']},
            ],
            "variants":[
                {
                    "title":item.Title ? item.Title : '',
                    "material":item.Material ? item.Material : '',
                    "sku":item['Variant SKU'] ? item['Variant SKU'] : '',
                    "barcode":item['Variant Barcode'] ? item['Variant Barcode'] : '',
                    "weight":item['Variant Weight'] ? +item['Variant Weight'] : 0,
                    "inventory_quantity":item['Variant Inventory Qty'] ? +item['Variant Inventory Qty'] : 0,
                    "prices":[{"amount":item['Variant Price'] ? Math.floor(Number(item['Variant Price'])) * 100 : 1000, "currency_code":"usd"}],
                    "allow_backorder":false,
                    "options":[
                                // {"value":item['Option1 Value']},
                                // {"value":item['Option2 Value']},
                                // {"value":item['Option3 Value']},
                            ],
                    "manage_inventory":true
                },
                        ],
            "status":"published",
            "images":[
                item['Variant Image'] ? item['Variant Image'] : '',
        ],
            "thumbnail":"",
        }
        if(item['DialedN PCAT'] && item['DialedN PCAT'] !== 'Need to add')prod.categories.push({id:item['DialedN PCAT']})
if (item['Option1 Name']) prod.options.push({"title": item['Option1 Name']})
if (item['Option2 Name']) prod.options.push({"title": item['Option2 Name']})
if (item['Option3 Name']) prod.options.push({"title": item['Option3 Name']})
if (item['Option1 Value']) prod.variants[0].options.push({"value":item['Option1 Value']})
if (item['Option2 Value']) prod.variants[0].options.push({"value":item['Option2 Value']})
if (item['Option3 Value']) prod.variants[0].options.push({"value":item['Option3 Value']})
return prod
}
    }


const masterList = {}
const fakeProduct = (i) => {
    const item = result[i]
    if(item.Handle){
        if(masterList[item.Handle] !== undefined){
            const current = masterList[item.Handle]
            if( [item['Option1 Name'], item['Option2 Name'], item['Option3 Name']].includes('Color')){
                let index = [item['Option1 Name'], item['Option2 Name'], item['Option3 Name']].indexOf('Color')
                // console.log(index)
                const color = item[`Option${index + 1} Value`]
                // console.log(color)
                if(current[color]){
                    // console.log(current[color])
                    const vari =  
                        {
                            "title":item.Title ? item.Title : '',
                            "material":item.Material ? item.Material : '',
                            "sku":item['Variant SKU'] ? item['Variant SKU'] : '',
                            "barcode":item['Variant Barcode'] ? item['Variant Barcode'] : '',
                            "weight":item['Variant Weight'] ? +item['Variant Weight'] : 0,
                            "inventory_quantity":item['Variant Inventory Qty'] ? +item['Variant Inventory Qty'] : 0,
                            "prices":[{"amount":item['Variant Price'] ? Math.floor(Number(item['Variant Price'])) * 100 : 1000, "currency_code":"usd"}],
                            "allow_backorder":false,
                            "options":[
                                        // {"value":item['Option1 Value']},
                                        // {"value":item['Option2 Value']},
                                        // {"value":item['Option3 Value']},
                                    ],
                            "manage_inventory":true
                        }
                
                if (item['Option1 Value']) vari.options.push({"value":item['Option1 Value']})
                if (item['Option2 Value']) vari.options.push({"value":item['Option2 Value']})
                if (item['Option3 Value']) vari.options.push({"value":item['Option3 Value']})
                // console.log("VARIII", vari.options)
                current[color].variants.push(vari)    
            }else{
                const prod = createProd(item)
                if(prod){
                    current[color] = createProd(item)
                }
                    
                    // current[color].push(item)
                }
            }else{
                const prod = createProd(item)
                if(prod){   
                    current['variant'] = createProd(item)
                }
  
            }
        }else{
            masterList[item.Handle] = {}
            const current = masterList[item.Handle]
            if( [item['Option1 Name'], item['Option2 Name'], item['Option3 Name']].includes('Color')){
                let index = [item['Option1 Name'], item['Option2 Name'], item['Option3 Name']].indexOf('Color')
                // console.log(index)
                const color = item[`Option${index + 1} Value`]
                // console.log(color)
                if(current[color]){
                    // console.log(current[color])
                    const vari =  
                        {
                            "title":item.Title ? item.Title : '',
                            "material":item.Material ? item.Material : '',
                            "sku":item['Variant SKU'] ? item['Variant SKU'] : '',
                            "barcode":item['Variant Barcode'] ? item['Variant Barcode'] : '',
                            "weight":item['Variant Weight'] ? +item['Variant Weight'] : 0,
                            "inventory_quantity":item['Variant Inventory Qty'] ? +item['Variant Inventory Qty'] : 0,
                            "prices":[{"amount":item['Variant Price'] ? Math.floor(Number(item['Variant Price'])) * 100 : 1000, "currency_code":"usd"}],
                            "allow_backorder":false,
                            "options":[
                                        // {"value":item['Option1 Value']},
                                        // {"value":item['Option2 Value']},
                                        // {"value":item['Option3 Value']},
                                    ],
                            "manage_inventory":true
                        }
                
                if (item['Option1 Value']) vari.options.push({"value":item['Option1 Value']})
                if (item['Option2 Value']) vari.options.push({"value":item['Option2 Value']})
                if (item['Option3 Value']) vari.options.push({"value":item['Option3 Value']})
                // console.log("VARIII", vari.options)
                current[color].variants.push(vari)       
                }else{
                    const prod = createProd(item)
                    if(prod){
                        current[color] = createProd(item)
                    }
                    // current[color].push(item)
                }
            }else{
                const prod = createProd(item)
                if(prod){
                    current['variant'] = createProd(item)
                }
                
            }
        }
    }

}

const seed = async () => {
for (let i = 1 ;i < 5000; i++){
    // console.log(i)
    const fake = fakeProduct(i)
    // csoneol.log(fake)
    try {
        // const product = await medusa.admin.products.create(fake)
        // const data = await product.json()
        // console.log(data)
        // arr.table.push(fake)
    }
    catch (e) {
        // console.log(e.response)
    }

}
let num = 1
Object.keys(masterList).forEach(async (product, i) => {
    const currentProduct = masterList[product]
    console.log("CURENT PRODUCT", Object.keys(currentProduct).length)
    Object.keys(currentProduct).forEach(async color => {
        // console.log("I should run for the number of products before the next current product")
        const newProduct = currentProduct[color]
        // console.log("About to be made productr", newProduct)
        // console.log("About to be made productr", newProduct.variants)
        // console.log("About to be made productr", newProduct)
        // console.log("The COLOR",color)
        newProduct.tags.push({value : `pg_${newProduct.handle}`})
            try {
                setTimeout(async () => {
                    console.log("I should run for each product in current product")
                    const product = await medusa.admin.products.create(newProduct)

                }, num * 1000)
                num ++
                // console.log("AFTER CREATE!!!!!!!!!!!~!!!!!!!",product)
                // const data = await product.json()
                // console.log(data)
                // arr.table.push(fake)
            }
            catch (e) {
                console.log(e.response)
            }
     
    })
})
}
seed()
// console.log(masterList)
  }
});
// const login = await medusa.admin.auth.createSession(
//     {
//         email: 'some@email.com',
//         password: 'password',
//     }
// )
// console.log(login.response.cookies)
// const get = await medusa.admin.auth.getSession()
// let arr = {
//     table :[]
// }

// const admin = new AdminAuthResource(medusa)
// medusa.admin.auth.createSession(
//     {
//         email: 'me@adnan.io',
//         password: 'password'
//     }
// )

// console.log(medusa.products)