import "reflect-metadata"
import { DataSource } from "./node_modules/typeorm"
const json = require('./product_category.json')
import { Currency, Store, Image, ProductOption, ProductOptionValue, Product, ProductVariant, PriceList, MoneyAmount, CustomerGroup, Item, Cart, Order, Customer, Return, Swap, ReturnItem, Address, LineItem, ReturnReason, ClaimOrder, ClaimItem, ClaimImage, ClaimTag, ShippingMethod, ShippingOption, Region, ShippingProfile, PaymentProvider, TaxProvider, FulfillmentProvider, ShippingOptionRequirement, LineItemTaxLine, ShippingMethodTaxLine, Fulfillment, FulfillmentItem, FulfillmentOption, Payment, TrackingLink, LineItemAdjustment, Discount, DiscountRule, DiscountCondition, ProductType, ProductTag, ProductCategory, ProductCollection, OrderEdit, OrderItemChange, PaymentCollection, GiftCard, PaymentSession, Refund, DraftOrder, GiftCardTransaction, ProductVariantInventoryItem, TaxRate, Country} from "@medusajs/medusa"
import faker from 'fakerjs'
const AppDataSource = new DataSource({
  type: "postgres",
  host: "localhost",
  port: 5432,
  database: "medusa-store-update",
  entities: [Product, Store, Currency,ProductVariant, Image, ProductOption, ProductOptionValue, MoneyAmount, PriceList, CustomerGroup, Customer, Address,Swap, Return, ReturnItem, Cart,LineItem, Order, Item, ReturnReason, ClaimOrder, ClaimItem, ClaimImage, ClaimTag, ShippingMethod, ShippingOption, Region, ShippingProfile, PaymentProvider, TaxProvider, FulfillmentProvider, ShippingOptionRequirement, LineItemTaxLine, ShippingMethodTaxLine, Fulfillment, FulfillmentItem, FulfillmentOption, Payment, TrackingLink, LineItemAdjustment, Discount, DiscountRule, DiscountCondition, ProductType, ProductTag, ProductCategory, ProductCollection, OrderEdit, OrderItemChange, PaymentCollection, GiftCard, PaymentSession, Refund, DraftOrder, GiftCardTransaction, ProductVariantInventoryItem, TaxRate, Country, ProductCategory],
  synchronize: false,
  logging: false,
})
AppDataSource.initialize()
  .then(() => {
      console.log("Data Source has been initialized!")
      seed()
  })
  .catch((err) => {
      console.error("Error during Data Source initialization", err)
  })
const storeRepository = AppDataSource.getRepository(Store)
async function seed() {
  const storeRepository = AppDataSource.getRepository(Store)
  await storeRepository.save({
    id: "store_01GY0KYNNSVMFCG24HXBAATWTX",
    name: "Medusa Store2",
    default_currency_code: "usd",
    default_sales_channel_id: "sc_01GY0KYNRH1DHNH9AXZJ05BMP8",
    commission_percentage: 10,
  })
  try {
    for (let i = 0; i < json.length; i++) {
        await AppDataSource
        .createQueryBuilder()
        .insert()
        .into(ProductCategory)
        .values([
            {
                id: json[i].id,
                name: json[i].name,
                handle: json[i].handle,
                parent_category_id: json[i].parent_category_id,
                rank: json[i].rank,
                is_active: json[i].is_active,
            },
        ])
        .execute()
        await AppDataSource
        .createQueryBuilder()
        .update(ProductCategory)
        .set({ "mpath": json[i].mpath })
        .where("id = :id", { id: json[i].id })
        .execute()
    }
  } catch (error) {
    console.log(error)
  }
  try {
    for (let i = 0; i < json.length; i++) {
        await AppDataSource
        .createQueryBuilder()
        .insert()
        .into(Product)
        .values([
            {
                title: 'test',
                handle: json[i].handle,
                rank: json[i].rank,
                is_active: json[i].is_active,
            },
            {
                id: json[i].id,
                name: json[i].name,
                handle: json[i].handle,
                parent_category_id: json[i].parent_category_id,
                rank: json[i].rank,
                is_active: json[i].is_active,
            },
            {
              id: json[i].id,
              name: json[i].name,
              handle: json[i].handle,
              parent_category_id: json[i].parent_category_id,
              rank: json[i].rank,
              is_active: json[i].is_active,
            },
            {
                id: json[i].id,
                name: json[i].name,
                handle: json[i].handle,
                parent_category_id: json[i].parent_category_id,
                rank: json[i].rank,
                is_active: json[i].is_active,
            },
            {
              id: json[i].id,
              name: json[i].name,
              handle: json[i].handle,
              parent_category_id: json[i].parent_category_id,
              rank: json[i].rank,
              is_active: json[i].is_active,
            },
            {
                id: json[i].id,
                name: json[i].name,
                handle: json[i].handle,
                parent_category_id: json[i].parent_category_id,
                rank: json[i].rank,
                is_active: json[i].is_active,
            },
        ])
        .execute()
        await AppDataSource
        .createQueryBuilder()
        .update(ProductCategory)
        .set({ "mpath": json[i].mpath })
        .where("id = :id", { id: json[i].id })
        .execute()
    }
  } catch (error) {
    console.log(error)
  }
}